msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2020-09-04 19:55-0400\n"
"PO-Revision-Date: 2016-08-08 23:08+0100\n"
"Last-Translator: Rui Cruz <xande6ruz@yandex.com>\n"
"Language-Team: \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: elements-f15.svg:452(format) elements-f14.svg:173(format)
#: elements-f13.svg:102(format) elements-f12.svg:445(format)
#: elements-f11.svg:48(format) elements-f10.svg:48(format)
#: elements-f09.svg:48(format) elements-f08.svg:206(format)
#: elements-f07.svg:91(format) elements-f06.svg:91(format)
#: elements-f05.svg:48(format) elements-f04.svg:48(format)
#: elements-f03.svg:48(format) elements-f02.svg:48(format)
#: elements-f01.svg:48(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: elements-f12.svg:710(tspan)
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random Ant e 4WD"

#: elements-f12.svg:721(tspan)
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "Imagem SVG criada por Andrew Fitzsimon"

#: elements-f12.svg:726(tspan)
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Cortesia da Open Clip Art Library"

#: elements-f12.svg:731(tspan)
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr "http://www.openclipart.org/"

# devia ser GRANDE mas não cabe na imagem do tutorial
#: elements-f04.svg:79(tspan)
#, no-wrap
msgid "BIG"
msgstr "GRA"

#: elements-f04.svg:93(tspan)
#, no-wrap
msgid "small"
msgstr "pequeno"

#: elements-f01.svg:88(tspan)
#, no-wrap
msgid "Elements"
msgstr "Elementos"

#: elements-f01.svg:101(tspan)
#, no-wrap
msgid "Principles"
msgstr "Princípios"

#: elements-f01.svg:114(tspan) tutorial-elements.xml:96(title)
#, no-wrap
msgid "Color"
msgstr "Cor"

#: elements-f01.svg:127(tspan) tutorial-elements.xml:33(title)
#, no-wrap
msgid "Line"
msgstr "Linha"

#: elements-f01.svg:140(tspan) tutorial-elements.xml:49(title)
#, no-wrap
msgid "Shape"
msgstr "Forma"

#: elements-f01.svg:153(tspan) tutorial-elements.xml:79(title)
#, no-wrap
msgid "Space"
msgstr "Espaço"

#: elements-f01.svg:166(tspan) tutorial-elements.xml:113(title)
#, no-wrap
msgid "Texture"
msgstr "Textura"

#: elements-f01.svg:179(tspan) tutorial-elements.xml:128(title)
#, no-wrap
msgid "Value"
msgstr "Valor"

#: elements-f01.svg:192(tspan) tutorial-elements.xml:65(title)
#, no-wrap
msgid "Size"
msgstr "Tamanho"

#: elements-f01.svg:205(tspan) tutorial-elements.xml:150(title)
#, no-wrap
msgid "Balance"
msgstr "Equilíbrio"

#: elements-f01.svg:218(tspan) tutorial-elements.xml:166(title)
#, no-wrap
msgid "Contrast"
msgstr "Contraste"

#: elements-f01.svg:231(tspan) tutorial-elements.xml:179(title)
#, no-wrap
msgid "Emphasis"
msgstr "Ênfase"

#: elements-f01.svg:244(tspan) tutorial-elements.xml:194(title)
#, no-wrap
msgid "Proportion"
msgstr "Proporção"

#: elements-f01.svg:257(tspan) tutorial-elements.xml:208(title)
#, no-wrap
msgid "Pattern"
msgstr "Padrão"

#: elements-f01.svg:271(tspan) tutorial-elements.xml:222(title)
#, no-wrap
msgid "Gradation"
msgstr "Gradação"

#: elements-f01.svg:284(tspan) tutorial-elements.xml:240(title)
#, no-wrap
msgid "Composition"
msgstr "Composição"

#: elements-f01.svg:297(tspan)
#, no-wrap
msgid "Overview"
msgstr "Visão geral"

#: tutorial-elements.xml:7(title)
msgid "Elements of design"
msgstr "Elementos do design"

#: tutorial-elements.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-elements.xml:13(para)
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Este tutorial demonstra os elementos e os princípios do design que são "
"normalmente ensinados a estudantes de arte para entender várias propriedades "
"na criação de obras gráficas. Isto não é uma lista completa, por isso pode "
"acrescentar, retirar e combinar para tornar este tutorial mais abrangente."

#: tutorial-elements.xml:29(title)
msgid "Elements of Design"
msgstr "Elementos do Design"

#: tutorial-elements.xml:30(para)
msgid "The following elements are the building blocks of design."
msgstr "Os elementos seguintes são os pilares do design."

#: tutorial-elements.xml:34(para)
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"Uma linha é definida como sendo uma marca com comprimento e direção, criada "
"por um ponto que se move através de uma superfície. Uma linha pode variar em "
"comprimento, espessura, direção, curvatura e cor. A linha pode ser "
"bidimensional (uma linha feita com lápis no papel) ou implicitamente "
"tridimensional."

#: tutorial-elements.xml:50(para)
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"Uma figura sólida, a forma é criada quando linhas verdadeiras ou implícitas "
"se encontram ao redor de um espaço. Uma mudança na cor ou no sombreado pode "
"definir uma forma. As formas podem ser divididas em vários tipos: geométrica "
"(quadrado, triângulo, círculo) e orgânica (irregular no contorno)."

#: tutorial-elements.xml:66(para)
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"Isto refere-se às variações nas proporções de objetos, linhas ou formas. "
"Existe uma variação de tamanhos nos objetos, quer real quer imaginária."

#: tutorial-elements.xml:80(para)
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"O espaço é a área vazia ou área aberta entre, ao redor, por cima, por baixo "
"ou dentro dos objetos. As figuras e formas são feitas pelo espaço ao redor e "
"dentro deles. O espaço é geralmente chamado de tridimensional ou "
"bidimensional. O espaço positivo é o que preenche uma figura ou forma. O "
"espaço negativo é o que rodeia uma figura ou forma."

#: tutorial-elements.xml:97(para)
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"A cor é o carácter percecionado de uma superfície de acordo com o "
"comprimento de onda da luz refletida da superfície. A cor tem 3 dimensões: "
"MATIZ (outra palavra para cor, indicada pelo seu nome como vermelho ou "
"amarelo), VALOR (quanto ao claro ou escuro), INTENSIDADE (quanto ao brilho "
"ou fosco)."

#: tutorial-elements.xml:114(para)
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"A textura é a maneira como se sente uma superfície (textura real) ou como "
"pode ser observada (textura implícita). As texturas são descritas por "
"palavras como por exemplo áspera, sedosa ou granulada."

#: tutorial-elements.xml:129(para)
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"O valor é quanto se vê algo como escuro ou claro. Realizam-se mudanças de "
"valor na cor adicionando preto ou branco a esta. Por exemplo, a técnica "
"tradicional chiaroscuro da pintura utiliza o alto contraste das luzes e "
"sombras numa composição."

#: tutorial-elements.xml:146(title)
msgid "Principles of Design"
msgstr "Princípios do Design"

#: tutorial-elements.xml:147(para)
msgid "The principles use the elements of design to create a composition."
msgstr "Os princípios usam os elementos do design para criar uma composição."

#: tutorial-elements.xml:151(para)
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"O equilíbrio é um sentido de igualdade visual numa forma, figura, valor, "
"cor, etc. O equilíbrio pode ser simétrico ou uniformemente equilibrado ou "
"assimétrico e não uniformemente equilibrado. Os objetos, valores, cores, "
"texturas, formas, figuras, etc. podem ser usados para criar um equilíbrio "
"numa composição."

#: tutorial-elements.xml:167(para)
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "O contraste é a justaposição de elementos opostos."

#: tutorial-elements.xml:180(para)
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"A ênfase é usada para destacar certas partes do trabalho artístico e chamar "
"a atenção do observador. O centro de interesse ou ponto de foco é o lugar "
"para onde se olha primeiro."

#: tutorial-elements.xml:195(para)
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"A proporção descreve o tamanho, posição ou quantidade de uma coisa comparada "
"à outra."

#: tutorial-elements.xml:209(para)
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"O padrão é criado repetindo-se um elemento (linha, forma ou cor) várias "
"vezes."

#: tutorial-elements.xml:223(para)
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"A gradação de tamanho e direção produz uma perspetiva linear. A gradação de "
"cores de quente para frio e tons escuros para claros produzem uma perspetiva "
"aérea. A gradação pode adicionar interesse e movimento a uma forma. Uma "
"gradação do escuro para o claro fará o olho movimentar-se ao longo de uma "
"forma."

#: tutorial-elements.xml:241(para)
msgid "The combining of distinct elements to form a whole."
msgstr "A combinação de elementos distintos para formar um todo."

#: tutorial-elements.xml:251(title)
msgid "Bibliography"
msgstr "Bibliografia"

#: tutorial-elements.xml:252(para)
msgid "This is a partial bibliography used to build this document."
msgstr "Isto é uma bibliografia parcial utilizada na concepção este documento."

#: tutorial-elements.xml:255(ulink)
msgid "http://www.makart.com/resources/artclass/EPlist.html"
msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#: tutorial-elements.xml:258(ulink)
msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#: tutorial-elements.xml:261(ulink)
msgid "http://www.johnlovett.com/test.htm"
msgstr "http://www.johnlovett.com/test.htm"

#: tutorial-elements.xml:265(ulink)
msgid "http://digital-web.com/articles/elements_of_design/"
msgstr "http://digital-web.com/articles/elements_of_design/"

#: tutorial-elements.xml:269(ulink)
msgid "http://digital-web.com/articles/principles_of_design/"
msgstr "http://digital-web.com/articles/principles_of_design/"

#: tutorial-elements.xml:274(para)
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""
"Agradecimentos especiais para Linda Kim (<ulink url=\"http://www.coroflot."
"com/redlucite/\">http://www.coroflot.com/redlucite/</ulink>) por me ajudar "
"(<ulink url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) neste "
"tutorial. Agradecimentos também ao Open Clip Art Library (<ulink url="
"\"http://www.openclipart.org/\">http://www.openclipart.org/</ulink>) e para "
"os artistas gráficos que têm contribuído para esse projeto."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-elements.xml:0(None)
msgid "translator-credits"
msgstr ""
"Thiago Pimentel <thiago.merces@gmail.com>, 2006\n"
"Rui Cruz <xande6ruz@yandex.com>, 2016"

#~ msgid "http://sanford-artedventures.com/study/study.html"
#~ msgstr "http://sanford-artedventures.com/study/study.html"

#~ msgid "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
#~ msgstr "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
